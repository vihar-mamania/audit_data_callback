FROM 186266557194.dkr.ecr.us-east-1.amazonaws.com/mvn-base:20210309 AS builder
COPY ./pom.xml /opt/appzen/
COPY ./settings.xml /opt/appzen/
RUN mvn -s /opt/appzen/settings.xml -f /opt/appzen/pom.xml dependency:go-offline
COPY . /opt/appzen/
RUN mvn -s /opt/appzen/settings.xml -f /opt/appzen/pom.xml clean package

FROM 186266557194.dkr.ecr.us-east-1.amazonaws.com/java-base:20201116 AS runtime
WORKDIR /opt/appzen
COPY --from=builder /opt/appzen/target/audit-data-callback-*.jar /opt/appzen/lib/audit-data-callback.jar
COPY container-startup /opt/appzen/bin/run
USER root
RUN chown -R appzen:appzen /opt/appzen && \
    chmod 555 /opt/appzen/bin/run
USER appzen
CMD [ "/opt/appzen/bin/run" ]
EXPOSE 8080:8080

# ARG variables must match build environment variables or variables created in build script
# ARG variables must havedefaultvalues so developers are not required to set via cli
# All built time environment variables are accessible
# Build script currently creates: BUILD_DATE, REPOSITORY, VERSION
# Labels definitions at http://label-schema.org
# Another idea on standard labels: https://github.com/projectatomic/ContainerApplicationGenericLabels
ARG BUILD_DATE=2000-01-01
ARG BUILD_NUMBER=1
ARG BUILD_TAG=dev
ARG BUILD_URL=dev
ARG GIT_COMMIT=dev
ARG GIT_URL=dev
ARG JOB_NAME=dev
ARG REPOSITORY=dev/proj
ARG VERSION=0.0.1
LABEL org.label-schema.build-date="${BUILD_DATE}"\
 org.label-schema.name="${REPOSITORY}"\
 org.label-schema.description="Public Expense Audit Results Service"\
 org.label-schema.vendor="AppZen"\
 org.label-schema.version="${VERSION}"\
 org.label-schema.vcs-ref="${GIT_COMMIT}"\
 org.label-schema.vcs-url="${GIT_URL}"\
 org.label-schema.schema-version="1.0"\
 com.appzen.jenkins-job="${JOB_NAME}"\
 com.appzen.jenkins-build="${BUILD_NUMBER}"\
 com.appzen.jenkins-build-tag="${BUILD_TAG}"\
 com.appzen.jenkins-build-url="${BUILD_URL}"
