package com.appzen.audit.callback.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Slf4j
@Component
@Order(1)
@WebFilter
public class AuthenticationFilter implements Filter {
    private static final String HEADER_X_API_KEY = "x-api-key";
    private static final String[] whiteList = {
            "api-docs",
            "swagger",
            "actuator"};

    @Value("${AUDIT_CALLBACK_AUTHN_API_KEY:}")
    private String authApiKey;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        if (isURIInWhitelist(req.getRequestURI(), whiteList)) {
            filterChain.doFilter(request, response);
            return;
        }
        String xApiKey = req.getHeader(HEADER_X_API_KEY);
        if (xApiKey == null || xApiKey.isEmpty() || !xApiKey.equals(authApiKey))
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "The api key is not valid.");
        else
            filterChain.doFilter(request, response);
    }

    public static boolean isURIInWhitelist(String inputStr, String[] items) {
        return Arrays.stream(items).parallel().anyMatch(inputStr::contains);
    }

    @Override
    public void destroy() {

    }
}
