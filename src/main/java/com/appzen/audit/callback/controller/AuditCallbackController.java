package com.appzen.audit.callback.controller;

import com.appzen.backend.audit.AutomatedWorkflowAction;
import com.appzen.backend.concur.ConcurApi;
import com.appzen.backend.expensereport.ExpenseReportBo;
import com.appzen.logging.annotation.InstrumentAPI;
import com.appzen.rest.auditcallback.service.AuditCallbackService;
import com.appzen.util.ReportFailureMonitoringConstants;
import com.appzen.util.ReportMonitoringConstants;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;

import static com.appzen.util.ReportFailureMonitoringConstants.API_TIMEOUT;
import static com.appzen.util.ReportFailureMonitoringConstants.AUTOMATED_AUDIT_CALLBACK_FAILED;
import static com.appzen.util.ReportFailureMonitoringConstants.SOURCE_APPZEN;
import static com.appzen.util.ReportFailureMonitoringConstants.SOURCE_CONCUR;
import static net.logstash.logback.argument.StructuredArguments.keyValue;


@RestController
@RequestMapping("/auditcallback")
@Slf4j
public class AuditCallbackController {

    @Autowired
    private AuditCallbackService auditCallbackService;
    @Autowired
    private ExpenseReportBo expenseReportBo;
    private static final String CUSTOMER_ID = "cust_id";
    private static final String REPORT_ID = "report_id";

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @InstrumentAPI
    public ResponseEntity addAuditedExpenseReport(@RequestBody String inputPayload) {
        log.debug("Processing audit callback with payload: {}", inputPayload);
        HashMap<String, String> resMap = new HashMap<>();
        String reportId = null;
        Long customerId = null;
        boolean isResubmitReport = false;
        try {
				JSONObject inputJSON = new JSONObject(inputPayload);
            String inputCustomerId = inputJSON.getString(CUSTOMER_ID);
            reportId = inputJSON.getString(REPORT_ID);
            customerId = Long.valueOf(inputCustomerId);
            isResubmitReport = expenseReportBo.isReportResubmit(customerId, reportId);
            auditCallbackService.processAuditedExpenseReport(customerId, reportId, inputPayload);
        } catch (AutomatedWorkflowAction.IllegalWorkflowActionException exception) {
            if (isResubmitReport) {
                ReportMonitoringConstants.addMDCResubmittedReportStateFluentdIdKey(String.valueOf(customerId), reportId);
            } else {
                ReportMonitoringConstants.addMDCReportStateFluentdIdKey(String.valueOf(customerId), reportId);
            }
            ReportMonitoringConstants.addMDCWorkflowFailed();
            log.info(ReportMonitoringConstants.MONITOR_WORKFLOW_FAILED);
            ReportMonitoringConstants.removeMDCWorkflowFailed();

            resMap.put("Code", "400");
            resMap.put("Message", exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resMap);
        } catch (ConcurApi.ConcurAPITimeoutException ex) {
            ReportFailureMonitoringConstants.addMDCAutomatedWorkflowFailure(SOURCE_CONCUR, API_TIMEOUT);
            log.warn("Api timeout while accessing concur endpoint: {}", ex.getMessage());
            ReportFailureMonitoringConstants.removeMDCAutomatedWorkflowFailure();

            log.error("Error while connecting to expense end point for customer {} and report {} because of {}", customerId, reportId, ex.getMessage());
        } catch (Exception e) {
            ReportFailureMonitoringConstants.addMDCAutomatedWorkflowFailure(SOURCE_APPZEN, AUTOMATED_AUDIT_CALLBACK_FAILED);
            log.warn("Failure while processing audit callback: {}", e.getMessage());
            ReportFailureMonitoringConstants.removeMDCAutomatedWorkflowFailure();

            if (isResubmitReport) {
                ReportMonitoringConstants.addMDCResubmittedReportStateFluentdIdKey(customerId, reportId);
            } else {
                ReportMonitoringConstants.addMDCReportStateFluentdIdKey(customerId, reportId);
            }
            ReportMonitoringConstants.addMDCWorkflowFailed();
            log.info(ReportMonitoringConstants.MONITOR_WORKFLOW_FAILED);
            ReportMonitoringConstants.removeMDCWorkflowFailed();

            log.error("Audit callback failed {} {} {}", customerId, reportId, keyValue("responseCode", 500), e);
            resMap.put("Code", "500");
            resMap.put("Message", Arrays.toString(e.getStackTrace()));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resMap);
        }
        resMap.put("Code", "200");
        resMap.put("Message", "Successfully");
        if (isResubmitReport) {
            ReportMonitoringConstants.addMDCResubmittedReportStateFluentdIdKey(customerId, reportId);
        } else {
            ReportMonitoringConstants.addMDCReportStateFluentdIdKey(customerId, reportId);
        }
        ReportMonitoringConstants.addMDCWorkflowSuccess();
        log.info(ReportMonitoringConstants.MONITOR_WORKFLOW_SUCCESS);
        ReportMonitoringConstants.removeMDCWorkflowSuccess();
        MDC.clear();
        return ResponseEntity.ok(resMap);
    }
}
