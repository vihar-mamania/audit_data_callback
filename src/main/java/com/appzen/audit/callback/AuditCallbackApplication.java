package com.appzen.audit.callback;

import com.appzen.mysql_data_service.dao.UserAuthNAuthZDao;
import com.appzen.services.ingestion_service.IngestionQueueManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        FlywayAutoConfiguration.class,
        QuartzAutoConfiguration.class,
        UserDetailsServiceAutoConfiguration.class
})
@ComponentScan(
        basePackages = {"com.appzen"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = UserAuthNAuthZDao.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = IngestionQueueManager.class)}
)
@Slf4j
public class AuditCallbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuditCallbackApplication.class, args);
    }

}
